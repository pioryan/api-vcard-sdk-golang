package main

import opsdk "bitbucket.org/matchmove/api-vcard-sdk-golang/sdk"

var credentials = struct {
	host, key, secret, username, password string
}{
	"",
	"",
	"",
	"",
	"",
}

// public resource demo
func getEnumerationTitles() (string, error) {
	api := opsdk.API{}

	if err := api.Connection(credentials.host, credentials.key, credentials.secret); err != nil {
		return "", err
	}

	// Consume returns *http.Response, string, error
	_, result, err := api.Consume("users/enumerations/titles", "GET", nil)
	if err != nil {
		return "", err
	}

	return result, nil
}

// authenticated resource demo
func getUserDetails() (string, error) {
	api := opsdk.API{}

	if err := api.Connection(credentials.host, credentials.key, credentials.secret); err != nil {
		return "", err
	}

	// authenticate
	if err := api.Authenticate(credentials.username, credentials.password); err != nil {
		return "", err
	}

	_, result, err := api.Consume("users", "GET", nil)
	if err != nil {
		return "", err
	}

	return result, nil
}
