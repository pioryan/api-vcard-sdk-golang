The Go Programming Language (1.7.1) SDK
==============

- version: 1.0
- compatible with: API version 1.3
- __Minimum__ Go version: 1.6

Required Packages:
------------------

- Standard Library

Dependencies:
-------------
- Standard Library


Installation:
-------------

1. Install the sdk using go get.

        go get "bitbucket.org/matchmove/api-vcard-sdk-golang/sdk"

2. Import into your go app with a preferred (optional) alias.

        import opsdk "bitbucket.org/matchmove/api-vcard-sdk-golang/sdk"

Usage:
------



### Initializing a `Connection` object

    :::go
    wallet := opsdk.API{}
   apit.Connection(host, consumerKey, consumerSecret)

### Consuming public resources

    :::go
    wallet := opsdk.API{}
    wallet.Connection(host, consumerKey, consumerSecret)

    // List all card types.
    _, cardTypes, _ := wallet.Consume("users/wallets/cards/types", "GET", nil)

    // Register a user.
    userData := map[string]string{
        "email": "me@email.com",
        "password": "password12",
        "first_name": "John",
        "last_name": "Doe",
        "mobile_country_code": "65",
        "mobile": "98765432",
    }

    _, user, _ := wallet.consume("users", "POST", userData)

### "Logging in" / User authentication

    :::go
    wallet := opsdk.API{}
    wallet.Connection(host, consumerKey, consumerSecret)
    wallet.Authenticate(user, password)


### Consume Authenticated resources

    :::go
    wallet := opsdk.API{}
    wallet.Connection(host, consumerKey, consumerSecret)
    wallet.Authenticate(user, password)

    // Get user details
    _, user, _ := wallet.Consume("users", "GET", nil)

    // Update user's details
	userData := map[string]string {
        "title": "Mr",
        "gender": "male",
        "id_number": "s1234567890",
        "id_type": "nric",
        "country_of_issue": "Singapore",
    }

    _, userUpdated, _ := wallet.Consume("users", "PUT", userData)

More Examples:
--------------

- [demo.go](https://bitbucket.org/matchmove/api-vcard-sdk-golang/src/48fe22d66afa84997b622428dd40da7381b8e1c3/demo.go?at=master&fileviewer=file-view-default)
- [demo_test.go](https://bitbucket.org/matchmove/api-vcard-sdk-golang/src/48fe22d66afa84997b622428dd40da7381b8e1c3/demo_test.go?at=master&fileviewer=file-view-default)
- run "go test -v"

Issues
------

[Any inquiries, issues or feedbacks?](https://bitbucket.org/matchmove/api-vcard-sdk-golang/issues)
