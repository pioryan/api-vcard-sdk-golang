package main

import (
	"log"
	"testing"
)

func TestGetEnumerationTitles(t *testing.T) {
	result, err := getEnumerationTitles()
	if err != nil {
		t.Errorf("Test error. Failed to retrieve enumeration titles from public resource: %s", err)
	} else {
		log.Printf(result)
	}
}

func TestGetUserDetails(t *testing.T) {
	result, err := getUserDetails()
	if err != nil {
		t.Errorf("Test error. Failed to retrieve user details from authenticated resource: %s", err)
	} else {
		log.Printf(result)
	}
}
