package sdk

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// Token deals with required Oauth token data
type Token struct {
	OAuthToken       string `json:"oauth_token,omitempty"`
	OAuthTokenSecret string `json:"oauth_token_secret,omitempty"`
}

const (
	tokenRequestURI = "oauth/request/token"
	tokenAccessURI  = "oauth/access/token"
	tokenMethod     = "POST"
	nonceLen        = 32
	oAuthVersion    = "1.0"
)

// RequestToken initiates the first leg of the OAuth application flow
func (api *API) RequestToken() error {
	api.Params = map[string]string{}
	api.Params["oauth_consumer_key"] = api.Creds.Key
	api.Params["oauth_nonce"] = GetNonce()
	api.Params["oauth_timestamp"] = GetTimestamp()
	api.Params["oauth_signature_method"] = signatureMethod
	api.Params["oauth_user_name"] = api.User.Login
	api.Params["oauth_user_password"] = api.User.Password
	api.Params["oauth_version"] = oAuthVersion

	// generate signature
	api.Method = http.MethodPost
	api.Resource = tokenRequestURI
	signature, err := api.Sign(Encode(api.Creds.Secret))
	if err != nil {
		return fmt.Errorf("Request token error: %s", err)
	}

	api.Params["oauth_signature"] = signature

	// encrypt and replace user data in the params
	key := Hash{api.Params["oauth_signature"] + api.Creds.Secret}.MD5()
	e := Encryption{}
	api.Params["oauth_user_name"], _ = e.EncryptString(key, api.User.Login)
	api.Params["oauth_user_password"], _ = e.EncryptString(key, api.User.Password)

	// send the request
	_, body := api.Request()
	api.Token = Token{}
	if err := json.Unmarshal([]byte(body), &api.Token); nil != err {
		return fmt.Errorf("Token Request failed to get a response: %s", err)
	}

	log.Printf("REQUEST TOKEN: %v", api.Token)
	return nil
}

// AccessToken initiates the second leg of the OAuth application flow
func (api *API) AccessToken() error {
	if err := api.RequestToken(); nil != err {
		log.Printf("Token Access failed on first leg: %s", err)
		return err
	}

	api.Resource = tokenAccessURI

	// remove user data with a token from the first leg, then sign
	delete(api.Params, "oauth_user_name")
	delete(api.Params, "oauth_user_password")
	delete(api.Params, "oauth_signature")
	api.Params["oauth_nonce"] = GetNonce()
	api.Params["oauth_timestamp"] = GetTimestamp()
	api.Params["oauth_token"] = api.Token.OAuthToken
	secret := fmt.Sprintf("%s&%s", Encode(api.Creds.Secret), Encode(api.Token.OAuthTokenSecret))

	signature, err := api.Sign(secret)
	if err != nil {
		return fmt.Errorf("Access token error: %s", err)
	}

	api.Params["oauth_signature"] = signature

	// send the request
	_, body := api.Request()
	if err := json.Unmarshal([]byte(body), &api.Token); nil != err {
		log.Printf("Token Request failed to get a response: %s", err)
		return err
	}

	log.Printf("ACCESS TOKEN: %v", api.Token)
	return nil
}

// GetNonce creates a random alphanumeric string to use as OAuth nonce
func GetNonce() string {
	rand.Seed(time.Now().UnixNano())
	var letterRunes = []rune("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, nonceLen)

	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}

	return string(b)
}

// GetTimestamp returns a unix timestamp
func GetTimestamp() string {
	return strconv.FormatInt(time.Now().UTC().Unix(), 10)
}

// IsURL exhaustively tells you whether a string is a valid URL
// from github.com/asaskevich/govalidator
func IsURL(str string) bool {
	if str == "" || len(str) >= 2083 || len(str) <= 3 || strings.HasPrefix(str, ".") {
		return false
	}
	u, err := url.Parse(str)
	if err != nil {
		return false
	}
	if strings.HasPrefix(u.Host, ".") {
		return false
	}
	if u.Host == "" && (u.Path != "" && !strings.Contains(u.Path, ".")) {
		return false
	}
	if u.Scheme == "" {
		return false
	}

	IP := `(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|` +
		`([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:)` +
		`{1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|` +
		`:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|` +
		`1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9])` +
		`{0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))`
	URLSchema := `((ftp|tcp|udp|wss?|https?):\/\/)`
	URLUsername := `(\S+(:\S*)?@)`
	URLPath := `((\/|\?|#)[^\s]*)`
	URLPort := `(:(\d{1,5}))`
	URLIP := `([1-9]\d?|1\d\d|2[01]\d|22[0-3])(\.(1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.([0-9]\d?|1\d\d|2[0-4]\d|25[0-4]))`
	URLSubdomain := `((www\.)|([a-zA-Z0-9]([-\.][a-zA-Z0-9]+)*))`
	URL := `^` + URLSchema + `?` + URLUsername + `?` + `((` + URLIP + `|(\[` + IP +
		`\])|(([a-zA-Z0-9]([a-zA-Z0-9-]+)?[a-zA-Z0-9]([-\.][a-zA-Z0-9]+)*)|(` + URLSubdomain +
		`?))?(([a-zA-Z\x{00a1}-\x{ffff}0-9]+-?-?)*[a-zA-Z\x{00a1}-\x{ffff}0-9]+)(?:\.([a-zA-Z\x{00a1}-\x{ffff}]{1,}))?))` +
		URLPort + `?` + URLPath + `?$`

	rxURL := regexp.MustCompile(URL)
	return rxURL.MatchString(str)
}
