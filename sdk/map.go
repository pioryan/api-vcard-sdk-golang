package sdk

import (
	"fmt"
	"net/url"
	"sort"
	"strings"
)

// ToURL creates an encoded URL string out of a string map
func ToURL(m map[string]string) string {
	key, val := SortMapByKey(m)
	params := url.Values{}
	for i := 0; i < len(key); i++ {
		params.Add(key[i], val[i])
	}
	return params.Encode()
}

// ToURLString creates an encoded URL string out of a string map
func ToURLString(m map[string]string) string {
	s := ""
	key, val := SortMapByKey(m)
	for i := 0; i < len(key); i++ {
		s += fmt.Sprintf("%s=%s", key[i], val[i])
		if i < len(key)-1 {
			s += "&"
		}
	}
	return s
}

// Encode string to RFC3986
func Encode(s string) string {
	e := url.Values{}
	e.Add("", s)
	return strings.Replace(e.Encode()[1:], "+", "%20", -1)
}

// EncodeDelimiters replaces the characters "=" and "&" for strings
// formed by ToURL where contents need not be re-encoded
func EncodeDelimiters(s string) string {
	s = strings.Replace(s, "&", "%26", -1)
	s = strings.Replace(s, "=", "%3D", -1)
	return s
}

// SortMapByKey sorts the keys of a map in alphabetical order
// since maps are designed to be ordered arbitrarily in Go
// we return two string slices, indexed accordingly
func SortMapByKey(m map[string]string) ([]string, []string) {
	size := len(m)
	mk := make([]string, 0, size)
	mv := make([]string, 0, size)
	// sort keys
	for k := range m {
		mk = append(mk, k)
	}
	sort.Strings(mk)

	// store the value in the same order
	for i := 0; i < size; i++ {
		key := mk[i]
		mv = append(mv, m[key])
	}

	return mk, mv
}
