package sdk

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
)

const encryptionSaltHeader = "Salted__"
const aesBlockSize = 16

// Encryption is used as namespace and storage for creds
type Encryption struct {
	Creds creds
}

type creds struct {
	key []byte
	iv  []byte
}

// EncryptString uses OpenSSL / AES-256-CBC encryption algorithm
func (e *Encryption) EncryptString(password, plaintextString string) (string, error) {

	salt := make([]byte, 8) // Generate an 8 byte salt
	_, err := io.ReadFull(rand.Reader, salt)
	if err != nil {
		return "", err
	}

	data := make([]byte, len(plaintextString)+aesBlockSize)
	copy(data[0:], encryptionSaltHeader)
	copy(data[8:], salt)
	copy(data[aesBlockSize:], plaintextString)

	if e.setCreds([]byte(password), salt); err != nil {
		return "", err
	}

	enc, err := e.encrypt(data)
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(enc), nil
}

func (e *Encryption) encrypt(data []byte) ([]byte, error) {
	padded, err := e.pkcs7Pad(data, aesBlockSize)
	if err != nil {
		return nil, err
	}

	c, err := aes.NewCipher(e.Creds.key)
	if err != nil {
		return nil, err
	}
	cbc := cipher.NewCBCEncrypter(c, e.Creds.iv)
	cbc.CryptBlocks(padded[aesBlockSize:], padded[aesBlockSize:])

	return padded, nil
}

// DecryptString decrypts a string using OpenSSL, AES-256-CBC
func (e *Encryption) DecryptString(password, encryptedBase64String string) ([]byte, error) {
	data, err := base64.StdEncoding.DecodeString(encryptedBase64String)
	if err != nil {
		return nil, err
	}

	salt, err := e.getSalt(data)
	if err != nil {
		return nil, err
	}

	if e.setCreds([]byte(password), salt); err != nil {
		return nil, err
	}
	return e.decrypt(data)
}

func (e *Encryption) decrypt(data []byte) ([]byte, error) {
	if len(data) == 0 || len(data)%aesBlockSize != 0 {
		return nil, fmt.Errorf("bad blocksize(%v), aesBlockSize = %v\n", len(data), aesBlockSize)
	}
	c, err := aes.NewCipher(e.Creds.key)
	if err != nil {
		return nil, err
	}
	cbc := cipher.NewCBCDecrypter(c, e.Creds.iv)
	cbc.CryptBlocks(data[aesBlockSize:], data[aesBlockSize:])
	out, err := e.pkcs7Unpad(data[aesBlockSize:], aesBlockSize)
	if out == nil {
		return nil, err
	}
	return out, nil
}

func (e *Encryption) getSalt(data []byte) ([]byte, error) {
	saltHeader := data[:aesBlockSize]
	if string(saltHeader[:8]) != encryptionSaltHeader {
		return nil, fmt.Errorf("Does not appear to have been encrypted with OpenSSL, salt header missing.")
	}
	return saltHeader[8:], nil
}

func (e *Encryption) setCreds(password, salt []byte) error {
	m := make([]byte, 48)
	prev := []byte{}
	for i := 0; i < 3; i++ {
		prev = e.hash(prev, password, salt)
		copy(m[i*16:], prev)
	}
	e.Creds = creds{key: m[:32], iv: m[32:]}
	return nil
}

func (e *Encryption) hash(prev, password, salt []byte) []byte {
	a := make([]byte, len(prev)+len(password)+len(salt))
	copy(a, prev)
	copy(a[len(prev):], password)
	copy(a[len(prev)+len(password):], salt)
	return e.md5sum(a)
}

func (e *Encryption) md5sum(data []byte) []byte {
	h := md5.New()
	h.Write(data)
	return h.Sum(nil)
}

func (e *Encryption) pkcs7Pad(data []byte, blocklen int) ([]byte, error) {
	if blocklen <= 0 {
		return nil, fmt.Errorf("invalid blocklen %d", blocklen)
	}
	padlen := 1
	for ((len(data) + padlen) % blocklen) != 0 {
		padlen = padlen + 1
	}

	pad := bytes.Repeat([]byte{byte(padlen)}, padlen)
	return append(data, pad...), nil
}

func (e *Encryption) pkcs7Unpad(data []byte, blocklen int) ([]byte, error) {
	if blocklen <= 0 {
		return nil, fmt.Errorf("invalid blocklen %d", blocklen)
	}
	if len(data)%blocklen != 0 || len(data) == 0 {
		return nil, fmt.Errorf("invalid data len %d", len(data))
	}
	padlen := int(data[len(data)-1])
	if padlen > blocklen || padlen == 0 {
		return nil, fmt.Errorf("invalid padding")
	}
	pad := data[len(data)-padlen:]
	for i := 0; i < padlen; i++ {
		if pad[i] != byte(padlen) {
			return nil, fmt.Errorf("invalid padding")
		}
	}
	return data[:len(data)-padlen], nil
}
