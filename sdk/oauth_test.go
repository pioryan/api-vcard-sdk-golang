// +build all oauth

package sdk

import (
	"fmt"
	"testing"
)

var testOauthData = []struct {
	host, key, secret string
	login, password   string
	mustErr           bool
}{
	{
		"", "", "",
		"", "",
		true,
	},
	{
		"https://beta-api.mmvpay.com", "fake", "fake",
		"invalid", "invalid",
		true,
	},
	{
		"https://beta-api.mmvpay.com/sgmc", "fake", "fake",
		"invalid", "invalid",
		true,
	},
	{ // set this to your own creds if you want it to succeed in this test
		"https://beta-api.mmvpay.com/sgmc/v1", "fake", "fake",
		"bamboo+8840949@matchmove.com", "password123", //random valid test user on sgmc uat
		true, // set to false if you want this data to succeed
	},
}

func TestGetTokens(t *testing.T) {
	for _, v := range testOauthData {
		api := API{
			Host: v.host,
			Creds: Creds{
				Key:    v.key,
				Secret: v.secret,
			},
			User: User{
				Login:    v.login,
				Password: v.password,
			},
		}

		var err error
		if IsURL(v.host) {
			err = api.AccessToken()
		} else {
			err = fmt.Errorf("Host: %s - is not a valid URL.", v.host)
		}

		if v.mustErr && err == nil {
			t.Errorf("Test error on Oauth. Expected error but found none. Data: %#v", v)
		}

		if !v.mustErr && err != nil {
			t.Errorf("Test error on Oauth. Expected no error but found error: %s ... Data: %#v", err, v)
		}
	}
}

func TestGetNonce(t *testing.T) {
	for i := 0; i < 20; i++ {
		a := GetNonce()
		b := GetNonce()

		if a == b {
			t.Errorf("Every call must generate a random string in: %s", "GetNonce")
		}
		// test even the unlikely
		if nonceLen != len(a) || nonceLen != len(b) {
			t.Errorf("Got %v and %v, Want %v in expected string length in: %s", len(a), len(b), nonceLen, "GetNonce")
		}
	}
}
