// +build all api

package sdk

import (
	"fmt"
	"testing"
)

// cascade through all test data
var testAPICreds = []struct {
	host, key, secret string
	mustError         bool
}{
	{ // missing
		"", "", "",
		true,
	},
	{ // no proto
		"beta-api.mmvpay.com/sgmc", "somekey", "somesecret",
		true,
	},
	{ // malformed
		"https//beta-api.mmvpay.com/sgmc/v1", "somekey", "somesecret",
		true,
	},
	{ // invalid key / secret
		"https://beta-api.mmvpay.com/sgmc/v1", "", "",
		true,
	},
	{ // test fail without valid key / secret, properly formatted but invalid key/secret
		"https://beta-api.mmvpay.com/sgmc/v1", "vyAvo98",
		"yCMlxbd4o44TRn",
		false,
	},
}

var testAPILogins = []struct {
	login, password string
	mustError       bool
}{
	{ // test blank
		"", "",
		true,
	},
	{ // test invalid
		"user", "pass",
		true,
	},
	{ // test properly formatted but invalid
		"bamboo+884094911@matchmove.com", "password",
		true,
	},
}

var testAPICalls = []struct {
	resource, method     string
	query                map[string]string
	mustLogin, mustError bool
}{
	{ // invalid args
		"", "", nil,
		false,
		true,
	},
	{ // invalid args
		"a", "b", nil,
		false,
		true,
	},
	{ // invalid resource
		"bamboo+9840949@matchmove.com", "GET", nil,
		false,
		true,
	},
	{ // malformed resource, leading slash
		"/users/enumerations/titles", "GET", nil,
		false,
		true,
	},
	{ // invalid method
		"users/enumerations/titles", "PUSH", nil,
		false,
		true,
	},
	{ // valid public resource, no login
		"users/enumerations/titles", "GET", nil,
		false,
		true, // forced to fail since no valid creds are used here
	},
	{ // valid public resource, no login
		"users/wallets/cards/types", "GET", nil,
		false,
		true, // forced to fail since no valid creds are used here
	},
	{ // valid authenticated resource, requires login
		"users", "GET", nil,
		true,
		true, // forced to fail since no valid creds are used here
	},
}

func TestAPI(t *testing.T) {
	for _, apiCreds := range testAPICreds {
		api := API{}

		// creds setting test
		credsErr := api.Connection(apiCreds.host, apiCreds.key, apiCreds.secret)
		if apiCreds.mustError && credsErr == nil {
			t.Errorf("Test error. Expected error with invalid API credential format but no error occurred. Data: %s, %s, %s",
				apiCreds.host, apiCreds.key, apiCreds.secret)
		}

		for _, apiLogins := range testAPILogins {
			// test logins
			loginErr := api.Authenticate(apiLogins.login, apiLogins.password)

			// login must fail if bad api creds
			if apiCreds.mustError && loginErr == nil {
				t.Errorf("Test error. Expected error with invalid API credentials on login but no error occured.")
			}

			if apiLogins.mustError && loginErr == nil {
				t.Errorf("Test error. Expected error with invalid API and user credentials on Authenticate but no error returned.")
			}

			if !apiCreds.mustError && !apiLogins.mustError && loginErr != nil {
				t.Errorf("Test error. Expected no error but error returned: %s", loginErr)
			}

			for _, apiCalls := range testAPICalls {
				// test api calls
				// this is not testable atm if host url is invalid, request will just panic
				if nil != credsErr {
					continue
				}

				response, _, apiErr := api.Consume(apiCalls.resource, apiCalls.method, apiCalls.query)

				if response != nil && response.StatusCode != 200 {
					apiErr = fmt.Errorf("Error code returned from API: %v", response.StatusCode)
				}

				if apiCalls.mustLogin && nil != loginErr && nil == apiErr {
					t.Errorf("Test error. Expected an error but no error occured.")
				}

				if !apiCalls.mustLogin {

					if apiCalls.mustError && apiErr == nil {
						t.Errorf("Test error. Expected an error but no error occured on api call. Data: %#v", apiCalls)
					}

					if !apiCalls.mustError && credsErr == nil && apiErr != nil {
						t.Errorf("Test error. Expected successful api call without login required to %s %s but error occurred instead: %s. Data: %#v", apiCalls.method, apiCalls.resource, apiErr, apiCalls)
					}
				}

				if apiCalls.mustLogin {

					if loginErr != nil && apiErr == nil {
						t.Errorf("Test error. Expected an error when invalid login on api call. Data: %#v", apiCalls)
					}

					if loginErr == nil && apiCalls.mustError && apiErr == nil {
						t.Errorf("Test error. Expected an error but no error occured on api call. Data: %#v", apiCalls)
					}

					if loginErr == nil && !apiCalls.mustError && apiErr != nil {
						t.Errorf("Test error. Expected successful api call %s %s with all valid values but error occurred instead: %s. Data: %#v",
							apiCalls.method, apiCalls.resource, apiErr, apiCalls)
					}
				}
			}
		}
	}
}
