// +build all hash

package sdk

import "testing"

var hashTests = []struct {
	plain, encodedMD5, encodedSha1, encodedHMACSha1      string
	expectMD5Match, expectSha1Match, expectHMACSha1Match bool // values produced using C#
}{
	{ // fail all
		"TestString",
		"hello", "random", "world",
		false, false, false,
	},
	{ // correct hash, case mismatch, fail all
		"You've got your big cheese. I've got my hash |",
		"DEF5EF4ECDF8566C7151B0E003520CFB", "26588E784854D9EEDCA804085572560FFE763DAD", "JNQQAJEJ9I5CHNZU5O57AZ9UNYQ=",
		false, false, false,
	},
	{ // empty string, correct hash, succeed
		"",
		"d41d8cd98f00b204e9800998ecf8427e", "da39a3ee5e6b4b0d3255bfef95601890afd80709", "7Kc6cC0yF3wzviEBQbTKjEIrVMc=",
		true, true, true,
	},
	{ // correct hash, case match, succeed
		"Test a slightly longer string and hash it.",
		"dda86393c8173bb0488650e2344797ec", "1b8a2d31c5c1261aaddde8389680f4771dd39912", "ZEgoipOPoR+3VXTphoLz/cnqf78=",
		true, true, true,
	},
}

func TestHashMD5(t *testing.T) {
	for _, want := range hashTests {
		got := Hash{want.plain}.MD5()
		if want.expectMD5Match && want.encodedMD5 != got {
			t.Errorf("Expected match but %s does not match %s", got, want.encodedMD5)
		}

		if !want.expectMD5Match && want.encodedMD5 == got {
			t.Errorf("Expected no match but %s matched %s", got, want.encodedMD5)
		}
	}
}

func TestHashSha1(t *testing.T) {
	for _, want := range hashTests {
		got := Hash{want.plain}.SHA1()
		if want.expectSha1Match && want.encodedSha1 != got {
			t.Errorf("Expected match but %s does not match %s", got, want.encodedSha1)
		}

		if !want.expectSha1Match && want.encodedSha1 == got {
			t.Errorf("Expected no match but %s matched %s", got, want.encodedSha1)
		}
	}
}

func TestHashHMACSha1(t *testing.T) {
	for _, want := range hashTests {
		got := Hash{want.plain}.HMACSHA1(want.encodedMD5)
		if want.expectHMACSha1Match && want.encodedHMACSha1 != got {
			t.Errorf("Expected match but %s does not match %s", got, want.encodedHMACSha1)
		}

		if !want.expectHMACSha1Match && want.encodedHMACSha1 == got {
			t.Errorf("Expected no match but %s matched %s", got, want.encodedHMACSha1)
		}
	}
}
