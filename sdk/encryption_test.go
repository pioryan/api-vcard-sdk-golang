// +build all encryption

package sdk

import (
	"bytes"
	"fmt"
	"os/exec"
	"testing"
)

var encryptionTests = []struct {
	key, data, preEncrypted  string
	mustMatch, mustPreEncErr bool
}{
	{ // data should not decrypt to the same string in pre-encrypted
		"tkey10", "A glittering gem...",
		"U2FsdGVkX1/oQ8WV7LiuOdRofwfsUmNhdg9J7daOgGSPmY/4E5/H/slZVloTcmjv",
		false,
		false,
	},
	{ // pre-encrypted is invalid, should throw an error
		"tkey11", "To go at a quick pace.",
		"U2fsdGVkX1+9bzCJ267+J5Y0vISX43C+qCN9cbJYHfiKinUj753C7JT4yvZz45dN",
		false,
		true,
	},
	{ // key altered, must throw an invalid padding err at pre-encrypted decrypt
		"tkey13...", "Rather be a bird than a FISH.",
		"U2FsdGVkX1+HOY+99nieAsJsoceZwVeZ+Ks3zRAbF5KoOUa1pUbT4KuGkQo33DZH",
		false,
		true,
	},
	{ // must match
		"tkey14", "Sixty-Four asks for bread.",
		"U2FsdGVkX1+0Gh9gvZBcgN5ZUVUGp8BCDIlyTd8r10HK4dyPL+4AQNclGniDrj5V",
		true,
		false,
	},
	{ // must match
		"tkey15", "Lake is a long way from here.",
		"U2FsdGVkX18VXclzfsj+7nowtplY15BOhr51IehXiFaoIrE+ciD+iGTAJQXTmvK6",
		true,
		false,
	},
	{ // preEncrypted altered case, must throw an error
		"tkey16", "This is a Japanese doll.",
		"U2FSDGVKX1/ABVM0RMW6VDOPGGXS2QPLDXOITAZLZU6IG7WFJ72Q4XX3LNIA1HQ0",
		false,
		true,
	},
	{ // preEncrypted string altered, should be invalid
		"tkey17", "Tom got a small piece of pie.",
		"U2fsdGVkX1++UaMepZEOZv/bjDvZIDkvK/dSiVRoZ0SYMuzCzBZBUj3RvfkGiWwS",
		false,
		true,
	},
	{ // must match
		"tkey18", "The apple revels in authority.",
		"U2FsdGVkX18+hT8y/mYDKq/LhbvYJc1MBfxPTSg6mQLdMoLHWAn9rk3hgdJXQyrj",
		true,
		false,
	},
	{ // must match
		"tkey19", "The book is on the table.",
		"U2FsdGVkX19g1OZ3N85GFHBKyN3CCKDDYZnyX9GcWA847BadFRmmZhJE21AtGj9E",
		true,
		false,
	},
	{ // must match
		"tkey20", "Don't step on the broken glass.",
		"U2FsdGVkX18K1WFIDPvY1b/2D+K6tAzi8ZIwHB31i6mcsGoIDKEsMkU4GLgbxZQu",
		true,
		false,
	},
}

func TestEncryptToDecrypt(t *testing.T) {
	for _, v := range encryptionTests {
		e := Encryption{}
		d := Encryption{}
		dPre := Encryption{}

		// encryption
		enc, err := e.EncryptString(v.key, v.data)
		if err != nil {
			t.Fatalf("Test error at encrypt: %s using data: %s", err, v.data)
		}

		// decryption of encrypted product
		dec, err := d.DecryptString(v.key, string(enc))
		if err != nil {
			t.Fatalf("Test error at decrypt: %s using data: %s", err, v.data)
		}

		// decryption of preEncrypted
		decPre, err := dPre.DecryptString(v.key, v.preEncrypted)
		if !v.mustPreEncErr && err != nil {
			t.Errorf("Test error at pre-encrypted decrypt: %s using data: %s", err, v.data)
		}

		if v.mustPreEncErr && err == nil {
			t.Errorf("Expected error but found none at pre-encrypted decrypt: %s using data: %s", err, v.data)
		}

		// decryption using command line openssl (only works if openssl installed in unix system)
		cmd := exec.Command("/bin/bash", "-c", fmt.Sprintf("echo \"%s\" | openssl aes-256-cbc -k %s -d -a", enc, v.key))
		var out bytes.Buffer
		cmd.Stdout = &out
		cmd.Stderr = &out
		err = cmd.Run()
		if err != nil {
			t.Errorf("OpenSSL expected no error but got: %s using %s", err, v.data)
		}

		gotEnc := enc
		gotDec := string(dec)
		gotDecPre := string(decPre)
		gotOpenssl := out.String()
		wantEnc := v.preEncrypted
		wantPlain := v.data

		if wantEnc == gotEnc {
			t.Errorf("Should be improbable for encrypted strings to be identical: '%s' == '%s'", gotEnc, wantEnc)
		}

		if v.mustMatch {
			// should always match
			if wantPlain != gotDec {
				t.Errorf("Decyption must match but '%s' != '%s'", gotDec, wantPlain)
			}

			// should always match
			if wantPlain != gotDecPre {
				t.Errorf("Decryption of pre-encrypted string must match but '%s' != '%s'", gotDecPre, wantPlain)
			}

			// should always match
			if wantPlain != gotOpenssl {
				t.Errorf("Decryption must match openssl command line decryption but '%s' != '%s'", gotOpenssl, wantPlain)
			}
		}

		if !v.mustMatch {
			if wantPlain == gotDecPre {
				t.Errorf("Decryption of pre-encrypted string must not match but '%s' == '%s'", gotDecPre, wantPlain)
			}
		}
	}
}
